﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.WebUtilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ReverseProxy
{
    public class Middleware
    {
        private readonly RequestDelegate Next;
        private readonly HttpClient HttpClient;

        public Middleware(RequestDelegate nextMiddleware)
        {
            Next = nextMiddleware;
        }

        public async Task Invoke(HttpContext context)
        {
            var targetUri = BuildUri(context.Request);
            if (targetUri == null)
            {
                await Next(context);
                return;
            }

            var targetRequestMessage = CreateMessage(context, targetUri);

            using (var responseMessage = await HttpClient.SendAsync(targetRequestMessage, HttpCompletionOption.ResponseHeadersRead, context.RequestAborted))
            {
                context.Response.StatusCode = (int)responseMessage.StatusCode;
                CopyResponseHeaders(context, responseMessage);
                await responseMessage.Content.CopyToAsync(context.Response.Body);
            }
        }

        private Uri BuildUri(HttpRequest request)
        {
            PathString remainingPath;
            if (request.Path.StartsWithSegments("/oculus-games", out remainingPath))
                return new Uri("https://www.oculus.com/experiences/quest" + remainingPath);

            if (request.Path.StartsWithSegments("/code-pen-io", out remainingPath))
                return new Uri("https://codepen.io/ruidovisual/pen" + remainingPath);

            throw new ArgumentException($"Cannot resolve this this requested path: {request.Path}");
        }

        private HttpRequestMessage CreateMessage(HttpContext context, Uri targetUri)
        {
            var requestMessage = new HttpRequestMessage();
            CopyContentAndHeaders(context, requestMessage);

            //targetUri = new Uri(QueryHelpers.AddQueryString(targetUri.OriginalString, new Dictionary<string, string>() { { "x", "y" } }));
            requestMessage.RequestUri = targetUri;
            requestMessage.Headers.Host = targetUri.Host;
            requestMessage.Method = GetMethod(context.Request.Method);

            return requestMessage;
        }

        private static HttpMethod GetMethod(string method)
        {
            if (HttpMethods.IsDelete(method)) return HttpMethod.Delete;
            if (HttpMethods.IsGet(method)) return HttpMethod.Get;
            if (HttpMethods.IsHead(method)) return HttpMethod.Head;
            if (HttpMethods.IsOptions(method)) return HttpMethod.Options;
            if (HttpMethods.IsPost(method)) return HttpMethod.Post;
            if (HttpMethods.IsPut(method)) return HttpMethod.Put;
            if (HttpMethods.IsTrace(method)) return HttpMethod.Trace;
            
            return new HttpMethod(method);
        }

        private void CopyResponseHeaders(HttpContext context, HttpResponseMessage responseMessage)
        {
            foreach (var header in responseMessage.Headers)
                context.Response.Headers[header.Key] = header.Value.ToArray();

            foreach (var header in responseMessage.Content.Headers)
                context.Response.Headers[header.Key] = header.Value.ToArray();
            
            context.Response.Headers.Remove("transfer-encoding");
        }

        private void CopyContentAndHeaders(HttpContext context, HttpRequestMessage requestMessage)
        {
            var requestMethod = context.Request.Method;

            if (HttpMethods.IsPost(requestMethod) || HttpMethods.IsPut(requestMethod) || HttpMethods.IsPatch(requestMethod))
            {
                var streamContent = new StreamContent(context.Request.Body);
                requestMessage.Content = streamContent;
            }

            foreach (var header in context.Request.Headers)
                requestMessage.Content?.Headers.TryAddWithoutValidation(header.Key, header.Value.ToArray());
        }
    }
}
